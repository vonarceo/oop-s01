// ============
// ACTIVITY 
// ============


//1 How do you create arrays in JS?

//let array = []

//2 How do you access the first character of an array?

//array[0]

//3 How do you access the last character of an array?

//array[array.length-1]

//4 What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array. 

//indexOf()

//5 What array method loops over all elements of an array, performing a user-defined function on each iteration?

//forEach()

//6.   What array method creates a new array with elements obtained from a user-defined function?

//map()

//7.   What array method checks if all its elements satisfy a given condition?

//every()

//8.   What array method checks if at least one of its elements satisfies a given condition?

//some()

//9.   True or False: array.splice() modifies a copy of the array, leaving the original unchanged.

//false

//10.   True or False: array.slice() copies elements from original array and returns these as a new array.

//true

// Activity No. 1

let students = [
	"Tony",
	"Peter",
	"Wanda",
	"Vision",
	"Loki"

];


function addToEnd(x, y){
	if(typeof(y) !== "string"){
		return "error - can only add strings to an array"
	}
	else{
		x.push(y);
		return x
	}
}

console.log(addToEnd(students, "Ryan"));
console.log(addToEnd(students, 054));


// Activity No. 2

function addToStart(x, y){
	if(typeof(y) !== "string"){
		return "error - can only add strings to an array"
	}
	else{
		x.unshift(y);
		return x
	}
}

console.log(addToStart(students, "Tess"));
console.log(addToStart(students, 120));

// Activity No. 3

function elementChecker(x, y){
	if(x.length === 0){
		return "error - can only add strings to an array"
	}

	if(x.includes(y) === true){
		return true
	}

	if(x.includes(y) === false){
		return false
	}
}


// Activity No. 4

function checkAllStringEnding(x, y){
	if(x.length === 0){
		return "error - array must NOT be empty"
	}

	let notString = x.every(name => {
		return typeof name === "string"
	})

	if(notString === false){
		return "error - all array elements must be strings"
	}

	if(typeof y !== "string"){
		return "error - 2nd argument must be of data type string"
	}
	if(y.length > 1){
		return "error - 2nd argument must be a single character"
	}

	let lastLetter = x.every(name => {
		return name[name.length -1] === y
	})

	return (lastLetter === true) ? true : false
}

console.log(checkAllStringEnding(students, "e"))


// Activity No. 5

function stringLengthSorter(x){
		if (x.some(name => typeof(name) !== 'string')){
			return 'error - all array elements must be strings'
		} else {
			return x.sort((a,b) => a.length - b.length)
		}
	}



// Activity No. 8

function randomPicker(x){
	
	return x[Math.floor(Math.random() * x.length)];
}







